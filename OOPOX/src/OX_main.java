
import java.util.InputMismatchException;
import java.util.Scanner;

public class OX_main {
	private Scanner kb = new Scanner(System.in);
	private int row ;
	private int col ;
	Board board = null ;
	PlayerOX o =null;
	PlayerOX x = null;
	public OX_main() {
		this.o = new PlayerOX('o');
		this.x = new PlayerOX('x');
	}
	public void run() {
		while(true) {
			this.runOnce();
			if(!askContinue()){
				return;
			}
		}
	}
	private boolean askContinue() {
		while(true) {
			System.out.println("Continue y/n ?");
			String ans = kb.next();
			if(ans.equals("n")) {
				return false;
			}else if(ans.equals("y")) {
				return true; 
			}
		}
	}
	private int getRandomNumber(int min,int max) {
		return (int)((Math.random()*(max-min))+min);
	}
	public void newGame() {
		if(getRandomNumber(1,100)%2==0) {
			this.board = new Board(o,x);
		}else {
			this.board = new Board(x,o);
		}
		
	}
	public void runOnce() {
		this.showWelcome();
		this.newGame();
		while(true) {
			this.showBoard();
			this.showTurn();
			this.inputRowCol();
			if(board.checkWin()) {
				showResult();
				this.showStat();
				return ;
			}
			
			board.switchPlayer();
		}
		
	}

	private void showResult() {
		if(board.getWinner()!=null) {
			showWin();				
		}else {
			showDraw();
		}
		
	}
	private void showDraw() {
		System.out.println("Drawwww");
		
	}
	private void showWin() {
		System.out.println(board.getWinner().getName()+" Winnnn");
		
	}
	private void showStat() {
		System.out.println(o.getName()+"(win,lose,draw):"+o.getWin()+o.getLose()+o.getDraw());
		System.out.println(x.getName()+"(win,lose,draw):"+x.getWin()+x.getLose()+x.getDraw());

	}
	private void inputRowCol() {
		try {
			while(true) {
				this.input();
				if(board.setRowCol(row,col)) {
					return ;
				}else {
					System.out.println("this row col is not empty");
				}
			}
		}catch(ArrayIndexOutOfBoundsException exception) {
		    System.out.println("ArrayIndexOutOfBoundsException exception");
		}
	}
	private void input() {
		while(true) {
			try {
				 System.out.print("Please in put row col: ");
					this.row = kb.nextInt();
					this.col = kb.nextInt();
					if(kb.hasNext()) {
						System.out.println("Please in put number!!!");
					}
			}catch(InputMismatchException iE){
				kb.next();
				System.out.println("Please in put number!!!"); 
			}
	   
		}
	}

	private void showTurn() {
		System.out.println(board.getCurrentPlayer().getName()+" Turn");
		
	}

	private void showBoard() {
		char[][] data=this.board.getData();
		for(int row=0; row<data.length;row++) {
			for(int col = 0;col<data[row].length;col++) {
				System.out.print(" "+data[row][col]);
			}
			System.out.println(" ");
		}
	}

	private void showWelcome() {
		System.out.println("Welcome OX Game!!!");
		
	}
	
	

}