
public class Board {
	char[][] data = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
	PlayerOX first;
	PlayerOX second;
	private PlayerOX currentPlayer;
	private PlayerOX winner;
	int lastRow;
	int lastCol;
	int countTurn=0;
	public Board(PlayerOX first, PlayerOX second) {
		
		this.first = first;
		this.second = second;
		this.currentPlayer = this.first;
	}
	public char[][] getData() {
		return data;
	}
	
	public PlayerOX getCurrentPlayer() {
		return currentPlayer;
	}
	
	public PlayerOX getWinner() {
		return winner ;
	}
	public void switchPlayer() {
		if(this.currentPlayer == first) {
			this.currentPlayer = second;
		}else {
			this.currentPlayer = first ;
		}
	}
	boolean setRowCol(int row, int col) {
		if(this.data[row-1][col-1] !='-') {
			return false ;
		}
		this.data[row-1][col-1] = currentPlayer.getName();
		lastCol = col-1;
		lastRow = row-1;
		countTurn++ ;
		return true; 
	}
	public boolean checkRow() {
		for(int col=0;col<this.data[lastRow].length;col++) {
			if(this.data[lastRow][col]!=currentPlayer.getName()) {
				return false ;
			}
		}
		return true ;
	}
	public boolean checkCol() {
		for(int row=0;row<this.data[lastCol].length;row++) {
			if(this.data[row][lastCol]!=currentPlayer.getName()) {
				return false ;
			}
		}
		return true ;
	}
	public boolean checkCross1() {
		for(int i=0;i<this.data.length;i++) {
			if(this.data[i][i]!=currentPlayer.getName()) {
				return false ;
			}
		}
		return true ;
	}
	public boolean checkCross2() {
		for(int i=0;i<this.data.length;i++) {
			if(this.data[i][this.data.length-i-1]!=currentPlayer.getName()) {
				return false ;
			}
		}
		return true ;
	}
	public void updateStat() {
		if(this.first==this.winner) {
			this.first.win();
			this.second.lose();
		}else if(this.second==this.winner) {
			this.first.lose();
			this.second.win();
		}else {
			this.first.draw();
			this.second.draw();
		}
	}
	public boolean checkWin() {
		
		if(checkCol()||checkRow()||checkCross1()||checkCross2()) {
			this.winner = currentPlayer;
			updateStat();
			return true ;
		}
		if(countTurn==9) {
			updateStat();
			return true;
		}
		return false ;
	}
}
