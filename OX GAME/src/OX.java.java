import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class OX {
	
	static ArrayList<Integer> playerPos = new ArrayList<Integer>();
	static ArrayList<Integer> cpuPos = new ArrayList<Integer>();

	public static void main(String[] args) {
		char [] [] board = {{' ','|',' ','|',' '},
							{'-','+','-','+','-'},
							{' ','|',' ','|',' '},
							{'-','+','-','+','-'},
							{' ','|',' ','|',' '}
							};
		System.out.println("Welcome to OX Game !!!");
		System.out.println("Player vs Cpu");
		System.out.println("Player = O");
		System.out.println("Cpu = X");
		printboard(board);
		while(true) {
			Scanner kb = new Scanner(System.in);
			System.out.print("Enter player slot (1-9):");
			int slot = kb.nextInt();
			while(playerPos.contains(slot) || cpuPos.contains(slot) ) {
				System.out.println("wrong slot! please Enter again:");
				slot = kb.nextInt();
			}
			Random random = new Random();
			placeChar(board,slot,"player");
			String result = Check();
			if(result.length() > 0) {
				printboard(board);
				System.out.println(result);
				break;
			}
			int cpuP = random.nextInt(9) + 1;
			while(playerPos.contains(cpuP) || cpuPos.contains(cpuP) ) {
				cpuP = random.nextInt(9) + 1;
			}
			placeChar(board,cpuP,"cpu");
			printboard(board);
			result = Check();
			if(result.length() > 0) {
				printboard(board);
				System.out.println(result);
				break;
			}
		}

	}
 public static void printboard(char [] [] board) {
	 for(char[] row : board ) {
			for(char r : row) {
				System.out.print(r);
			}
			System.out.println();
		}
}
 public static void placeChar(char[][] board,int slot, String player) {
	 char s = ' ';
	 if(player.equals("player")) {
		 s = 'O';
		 playerPos.add(slot);
	 }
	 else if(player.equals("cpu")) {
		 s = 'X';
		 cpuPos.add(slot);
	 }
	 
	 switch(slot) {
		case 1:
			board[0][0] = s;
			break;
		case 2:
			board[0][2] = s;
			break;
		case 3:
			board[0][4] = s;
			break;
		case 4:
			board[2][0] = s;
			break;
		case 5:
			board[2][2] = s;
			break;
		case 6:
			board[2][4] = s;
			break;
		case 7:
			board[4][0] = s;
			break;
		case 8:
			board[4][2] = s;
			break;
		case 9:
			board[4][4] = s;
			break;
		default:
			break;
		}
	 
}
 public static String Check() {
	
	 List topRow = Arrays.asList(1, 2, 3);
	 List midRow = Arrays.asList(4, 5, 6);
	 List botRow = Arrays.asList(7, 8, 9);
	 List leftCol = Arrays.asList(1, 4, 7);
	 List midCol = Arrays.asList(2, 5, 8);
	 List rightCol = Arrays.asList(3, 6, 9);
	 List cross1 = Arrays.asList(1, 5, 9);
	 List cross2 = Arrays.asList(7, 5, 3);
	 
	 
	 List<List> winConditions = new ArrayList<List>();
	 winConditions.add(topRow);
	 winConditions.add(midRow);
	 winConditions.add(botRow);
	 winConditions.add(leftCol);
	 winConditions.add(midCol);
	 winConditions.add(rightCol);
	 winConditions.add(cross1);
	 winConditions.add(cross2);
	 
	 for(List L : winConditions) {
		 if(playerPos.size() + cpuPos.size() == 9) {
			 return "draw!!";
		 }	 
		 if(playerPos.containsAll(L)) {
		 	return "Player Wins!";
		 }else if(cpuPos.containsAll(L)) {
			 return "Cpu wins! you lose..";
		 }
		 
	 }
	  	
	 
	 if(playerPos.size() + cpuPos.size() == 9) {
		return "draw!!";
 }
	 return "";
	 
 }
 }