package OOPOX;


public class Player {
	private char name;
	private int win;
	private int draw;
	private int lose;
	
	public int getWin() {
		return win;
	}
	
	public int getDraw() {
		return draw;
	}
	public int getLose() {
		return lose;
	}
	
	public void setName(char name) {
		this.name = name;
	}
	public Player(char name){
		this.name=name;
	}
	public char getName() {
		return name;
	}
	public void win() {
		this.win++;
	}
	public void lose() {
		this.lose++;
	}
	public void draw() {
		this.draw++;
	}

	
	
	public String toString() {
		return "Player{"+"name="+ name + '}';
		
	};
}