package OOPOX;

public class Table {
	char[][] data = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
	Player first;
	Player second;
	private Player currentPlayer;
	private Player winner;
	int lastRow;
	int lastCol;
	int countTurn=0;
	public Table(Player first, Player second) {
		
		this.first = first;
		this.second = second;
		this.currentPlayer = this.first;
	}
	public char[][] getData() {
		return data;
	}
	
	public Player getCurrentPlayer() {
		return currentPlayer;
	}
	
	public Player getWinner() {
		return winner ;
	}
	public void switchPlayer() {
		if(this.currentPlayer == first) {
			this.currentPlayer = second;
		}else {
			this.currentPlayer = first ;
		}
	}
	boolean setRowCol(int row, int col) {
		if(this.data[row-1][col-1] !='-') {
			return false ;
		}
		this.data[row-1][col-1] = currentPlayer.getName();
		lastCol = col-1;
		lastRow = row-1;
		countTurn++ ;
		return true; 
	}
	public boolean checkRow() {
		for(int col=0;col<this.data[lastRow].length;col++) {
			if(this.data[lastRow][col]!=currentPlayer.getName()) {
				return false ;
			}
		}
		return true ;
	}
	public boolean checkCol() {
		for(int row=0;row<this.data[lastCol].length;row++) {
			if(this.data[row][lastCol]!=currentPlayer.getName()) {
				return false ;
			}
		}
		return true ;
	}
	public boolean checkCross1() {
		for(int i=0;i<this.data.length;i++) {
			if(this.data[i][i]!=currentPlayer.getName()) {
				return false ;
			}
		}
		return true ;
	}
	public boolean checkCross2() {
		for(int i=0;i<this.data.length;i++) {
			if(this.data[i][this.data.length-i-1]!=currentPlayer.getName()) {
				return false ;
			}
		}
		return true ;
	}
	public boolean checkWin() {
		
		if(checkCol()||checkRow()||checkCross1()||checkCross2()) {
			this.winner = currentPlayer;
			return true ;
		}
		if(countTurn==9) {
			return true;
		}
		return false ;
	}

}
