package OOPOX;

import java.util.Scanner;
public class Game {
	
	private Scanner kb = new Scanner(System.in);
	private int row ;
	private int col ;
	Table table = null ;
	Player o =null;
	Player x = null;
	public Game() {
		this.o = new Player('o');
		this.x = new Player('x');
	}
	
	public void ShowTable() {
		System.out.println("Welcome To OXgame V.3");
	}

	public void run() {
		this.ShowTable();
		this.table = new Table(o,x);
		while(true) {
			this.showTable();
			this.showTurn();
			this.inputRowCol();
			if(table.checkWin()) {
				if(table.getWinner()!=null) {
					System.out.println(table.getWinner().getName()+" Winnnn");				}
				else {
					System.out.println("Drawwww");
				}
				return ;
			}
			table.switchPlayer();
		}

	}
	
	private void inputRowCol() {
		try {
			while(true) {
				this.input();
				if(table.setRowCol(row,col)) {
					return ;
				}else {
					System.out.println("======================");
					System.out.println("## this row col is not empty ##");
					System.out.println("======================");
				}
			}
		}catch(ArrayIndexOutOfBoundsException exception) {
			System.out.println("======================");
		    System.out.println("## this no on table ##");
		    System.out.println("======================");
		    this.inputRowCol();
		}
	}
	
	private void input() {
	    System.out.print("Please in put row col: ");
		this.row = kb.nextInt();
		this.col = kb.nextInt();
		
	}

	private void showTurn() {
		System.out.println(table.getCurrentPlayer().getName()+" Turn");
		
	}

	private void showTable() {
		System.out.println("======================");
		char[][] data=this.table.getData();
		for(int row=0; row<data.length;row++) {
			for(int col = 0;col<data[row].length;col++) {
				System.out.print(" "+data[row][col]);
			}
			System.out.println(" ");
		}
	}

	
}

